package com.leobit.solosdmanager;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.e.leobit.sdcarddownloader.SDCardManager;
import com.e.leobit.sdcarddownloader.UploadListener;

public class MainActivity extends AppCompatActivity {
    public static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SDCardManager.registerReceiver(this, new UploadListener() {
            @Override
            public void onUploadStarted() {
                Log.d(TAG, "onUploadStarted: ");
            }

            @Override
            public void onUploadFinished(boolean success) {
                Log.d(TAG, "onUploadFinished: success = " + success);
            }

            @Override
            public void onProgress(int currentProgress, int filesCount, int currentFile) {
                Log.d(TAG, "onProgress: current p : " + currentProgress + " filestotal " + filesCount + " currentfile " + currentFile);
            }
        });

//        SDCardManager.startCopying(new UploadListener() {
//            @Override
//            public void onUploadStarted() {
//                Log.d(TAG, "onUploadStarted: ");
//            }
//
//            @Override
//            public void onUploadFinished(boolean success) {
//                Log.d(TAG, "onUploadFinished: success = " + success);
//            }
//
//            @Override
//            public void onProgress(int progress) {
//                Log.d(TAG, "onProgress: " + progress);
//            }
//        }, this);
    }
}
