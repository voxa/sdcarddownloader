package com.e.leobit.sdcarddownloader;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.io.File;

import static com.e.leobit.sdcarddownloader.Constants.EXTERNAL_PATH;
import static com.e.leobit.sdcarddownloader.Constants.INTERNAL_PATH;
import static com.e.leobit.sdcarddownloader.Constants.STAGES;
import static com.e.leobit.sdcarddownloader.Constants.dirSize;

public class SDCardManager {
    public static final String TAG = SDCardManager.class.getSimpleName();
    private static SDMountedReceiver receiver;

    public static void registerReceiver(Context context, UploadListener uploadListener) {
        Log.d(TAG, "register SDMounted receiver");
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_MEDIA_MOUNTED);
        filter.addAction(Intent.ACTION_MEDIA_UNMOUNTED);
        receiver = new SDMountedReceiver(uploadListener, context);
        context.registerReceiver(receiver, filter);
    }

    public static void unregisterReceiver(Context context) {
        context.unregisterReceiver(receiver);
    }

    public static boolean checkStorage() {
        File sdCardStages = new File(EXTERNAL_PATH + "/" + STAGES);
        File internalStages = new File(INTERNAL_PATH + "/" + STAGES);
        if (sdCardStages.exists() && internalStages.exists()) {
            long externalStagesSize = dirSize(sdCardStages);
            long internalStagesSize = dirSize(internalStages);

            return (internalStages.exists() && (externalStagesSize == internalStagesSize));
        } else return false;
    }

    public static void startCopying(final UploadListener uploadListener, Context context) {
        BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Integer currentProgress = intent.getIntExtra("currentProgress", -1);
                Integer filesCount = intent.getIntExtra("filesCount", -1);
                Integer currentFile = intent.getIntExtra("currentFile", -1);
                if (currentProgress != -1 && filesCount != -1 && currentFile != -1) {
                    uploadListener.onProgress(currentProgress, filesCount, currentFile);
                }
                String start = intent.getStringExtra("start");
                if (start != null) {
                    uploadListener.onUploadStarted();
                }

                Integer finish = intent.getIntExtra("finish", -1);
                if (finish != -1) {
                    if (finish == 1) {
                        uploadListener.onUploadFinished(true);
                    } else if (finish == 0) {
                        uploadListener.onUploadFinished(false);
                    }
                }
            }
        };

        LocalBroadcastManager.getInstance(context).registerReceiver(mMessageReceiver,
                new IntentFilter("SD_CARD_MANAGER"));

        Log.d(TAG, "starting downloader service");

        if (isMyServiceRunning(Downloader.class, context)) {
            context.stopService(new Intent(context, Downloader.class));
        }

        context.startService(new Intent(context, Downloader.class));
    }

    private static boolean isMyServiceRunning(Class<?> serviceClass, Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
