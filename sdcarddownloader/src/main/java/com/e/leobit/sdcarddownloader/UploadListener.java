package com.e.leobit.sdcarddownloader;

public interface UploadListener {
    void onUploadStarted();

    void onUploadFinished(boolean success);

    void onProgress(int currentProgress, int filesCount, int currentFile);
}
