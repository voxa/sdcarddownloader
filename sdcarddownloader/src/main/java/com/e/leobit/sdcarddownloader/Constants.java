package com.e.leobit.sdcarddownloader;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

public class Constants {
    public static final String EXTERNAL_PATH = "/mnt/external_sd";
    public static final String INTERNAL_PATH = "/mnt/internal_sd";
    public static final String STAGES = "Stages";

    public static long UPDATE_DELAY = 1000; // Delay in millis

    public static int totalFilesCount = 0;

    public static long dirSize(File dir) {
        if (dir.exists()) {
            long result = 0;
            File[] fileList = dir.listFiles();
            for (int i = 0; i < fileList.length; i++) {
                // Recursive call if it's a directory
                if (fileList[i].isDirectory()) {
                    result += dirSize(fileList[i]);
                } else {
                    // Sum the file size in bytes
                    result += fileList[i].length();
                    ++totalFilesCount;
                }
            }
            return result; // return the file size
        }
        return 0;
    }

    public static long dirSize2(final File file) {
        if (file == null || !file.exists())
            return 0;
        if (!file.isDirectory())
            return file.length();
        final List<File> dirs = new LinkedList<>();
        dirs.add(file);
        long result = 0;
        while (!dirs.isEmpty()) {
            final File dir = dirs.remove(0);
            if (!dir.exists())
                continue;
            final File[] listFiles = dir.listFiles();
            if (listFiles == null || listFiles.length == 0)
                continue;
            for (final File child : listFiles) {
                result += child.length();
                if (child.isDirectory())
                    dirs.add(child);
            }
        }
        return result;
    }
}
