package com.e.leobit.sdcarddownloader;

import android.app.Service;
import android.content.Intent;
import android.os.Environment;
import android.os.IBinder;
import android.os.StatFs;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.security.MessageDigest;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static com.e.leobit.sdcarddownloader.Constants.EXTERNAL_PATH;
import static com.e.leobit.sdcarddownloader.Constants.INTERNAL_PATH;
import static com.e.leobit.sdcarddownloader.Constants.STAGES;
import static com.e.leobit.sdcarddownloader.Constants.UPDATE_DELAY;
import static com.e.leobit.sdcarddownloader.Constants.dirSize;

public class Downloader extends Service {
    public static final String TAG = Downloader.class.getSimpleName();
    private int totalSize = 0;
    private int currentFileCount = 0;
    int lastFile = 0;
    long downloaded = 0;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "service started");
        Constants.totalFilesCount = 0;
        totalSize = (int) (dirSize(new File(EXTERNAL_PATH + "/" + STAGES)) / 10000);

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                pushStartDownloading();
                File stagesDir = new File(EXTERNAL_PATH + "/" + STAGES);
                getAllFilesFromDir(stagesDir);

                int finished = (int) (dirSize(new File(INTERNAL_PATH + "/" + STAGES)) / 10000);

                Log.d(TAG, "exterSize " + totalSize + " localSize " + finished);
                //if (totalSize == finished) {
                pushFinishDownloading(1);
                //} else {
                //    pushFinishDownloading(0);
                //}
                Log.d(TAG, "Uploading finished...");
                stopSelf();
            }
        });

        thread.start();

        return START_STICKY;
    }

    private void copyFile(File srcFile, File destFile) throws IOException {
        if (srcFile == null) {
            throw new NullPointerException("Source must not be null");
        }
        if (destFile == null) {
            throw new NullPointerException("Destination must not be null");
        }
        if (!srcFile.exists()) {
//            throw new FileNotFoundException("Source '" + srcFile + "' does not exist");
        }
        if (srcFile.isDirectory()) {
            throw new IOException("Source '" + srcFile + "' exists but is a directory");
        }
        if (srcFile.getCanonicalPath().equals(destFile.getCanonicalPath())) {
            throw new IOException("Source '" + srcFile + "' and destination '" + destFile + "' are the same");
        }
        File parentFile = destFile.getParentFile();
        if (parentFile != null) {
            if (!parentFile.mkdirs() && !parentFile.isDirectory()) {
                throw new IOException("Destination '" + parentFile + "' directory cannot be created");
            }
        }
        if (destFile.exists() && !destFile.canWrite()) {
            throw new IOException("Destination '" + destFile + "' exists but is read-only");
        }

        if (destFile.exists()) {
            try {
                String hashValue = MD5HashFile(srcFile.getAbsolutePath());
                String hashValue1 = MD5HashFile(destFile.getAbsolutePath());

                Log.e(TAG, "checked " + srcFile.getName() + "sd card " + (dirSize(srcFile)) + " local" + dirSize(destFile)+" \n\n\n\n\n" +
                        "hash1 "+hashValue+" hash2 "+hashValue1+" equals "+(hashValue.equals(hashValue1)));

                if (hashValue.equals(hashValue1)) {
                    pushProgress(1, Constants.totalFilesCount, currentFileCount);
                    return;
                }
            } catch (Exception e) {
            }
        }

        boolean resultOfCoping = doCopyFile(srcFile, destFile);
        while (!resultOfCoping) {
            Log.d(TAG, "Copying of " + srcFile + " failed started one more time");
            resultOfCoping = doCopyFile(srcFile, destFile);
        }
    }

    private boolean doCopyFile(File srcFile, File destFile) throws IOException {
        if (destFile.exists() && destFile.isDirectory()) {
//            throw new IOException("Destination '" + destFile + "' exists but is a directory");

            return true;
        }

        StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
        long bytesAvailable = stat.getBlockSizeLong() * stat.getBlockCountLong();
        long bytesFile = srcFile.length();
        if (bytesFile > bytesAvailable) {
            Log.d(TAG, "doCopyFile: No available space left(");
            return false;
        } else {

            Log.d(TAG, "doCopyFile: Download started: " + srcFile.getName());

            long startTime = System.currentTimeMillis();

            try (InputStream in = new FileInputStream(srcFile)) {
                try (OutputStream out = new FileOutputStream(destFile)) {
                    // Transfer bytes from in to out
                    long total = 0;
                    byte[] buf = new byte[1024];
                    int len;
                    while ((len = in.read(buf)) > 0) {
                        total += len;
                        int progress = (int) ((total * 100) / srcFile.length());
//                        Log.d(TAG, "progress = " + progress);
//                        pushProgress(progress, Constants.totalFilesCount, currentFileCount);
                        out.write(buf, 0, len);

                        long currentTime = System.currentTimeMillis();
                        if ((currentTime - startTime > UPDATE_DELAY) || lastFile == currentFileCount) {
                            pushProgress(progress, Constants.totalFilesCount, currentFileCount);
                            startTime = currentTime;
                        }

                    }
                    lastFile = currentFileCount;
                } catch (IOException e) {
                    return false;
                }
            } catch (IOException e) {
                return false;
            }
        }
        return true;
    }

    private void getAllFilesFromDir(File directory) {
        Log.d(TAG, "Directory: " + directory.getAbsolutePath() + "\n");

        final File[] files = directory.listFiles();

        if (files != null) {
            for (File file : files) {
                if (file != null) {
                    if (file.isDirectory()) {  // it is a folder...
                        getAllFilesFromDir(file);
                    } else {  // it is a file...
                        ++currentFileCount;
                        pushProgress(0, Constants.totalFilesCount, currentFileCount);
                        Log.d(TAG, "File: " + file.getAbsolutePath() + "\n");
                        try {
                            copyFile(file, new File(file.getAbsolutePath().replace("external", "internal")));
                            downloaded += file.length();
                            int totalProgress = (int) ((downloaded * 100) / totalSize);
//                            pushProgress(totalProgress);
                            Log.d(TAG, "totalProgress = " + totalProgress);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }

    private static String MD5HashFile(String filename) throws Exception {
        byte[] buf = ChecksumFile(filename);
        String res = "";
        for (int i = 0; i < buf.length; i++) {
            res += Integer.toString((buf[i] & 0xff) + 0x100, 16).substring(1);
        }
        return res;
    }

    private static byte[] ChecksumFile(String filename) throws Exception {
        InputStream fis = new FileInputStream(filename);
        byte[] buf = new byte[1024];
        MessageDigest complete = MessageDigest.getInstance("MD5");
        int n;
        do {
            n = fis.read(buf);
            if (n > 0) {
                complete.update(buf, 0, n);
            }
        } while (n != -1);
        fis.close();
        return complete.digest();
    }

    private void pushProgress(Integer currentProgress, Integer filesCount, Integer currentFile) {
        Intent intent = new Intent("SD_CARD_MANAGER");
        intent.putExtra("currentProgress", currentProgress);
        intent.putExtra("filesCount", filesCount);
        intent.putExtra("currentFile", currentFile);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void pushStartDownloading() {
        Intent intent = new Intent("SD_CARD_MANAGER");
        intent.putExtra("start", "start");
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void pushFinishDownloading(int success) {
        Intent intent = new Intent("SD_CARD_MANAGER");
        intent.putExtra("finish", success);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }
}
