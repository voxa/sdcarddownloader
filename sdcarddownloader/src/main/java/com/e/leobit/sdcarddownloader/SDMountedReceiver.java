package com.e.leobit.sdcarddownloader;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

public class SDMountedReceiver extends BroadcastReceiver {
    public static final String TAG = SDMountedReceiver.class.getSimpleName();
    UploadListener uploadListener;

    public SDMountedReceiver() {
    }

    public SDMountedReceiver(final UploadListener uploadListener, Context context) {
        this.uploadListener = uploadListener;

        BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                Integer currentProgress = intent.getIntExtra("currentProgress", -1);
                Integer filesCount = intent.getIntExtra("filesCount", -1);
                Integer currentFile = intent.getIntExtra("currentFile", -1);
                if (currentProgress != -1 && filesCount != -1 && currentFile != -1) {
                    uploadListener.onProgress(currentProgress, filesCount, currentFile);
                }

                String start = intent.getStringExtra("start");
                if (start != null) {
                    uploadListener.onUploadStarted();
                }

                Integer finish = intent.getIntExtra("finish", -1);
                Log.d(TAG, "FINISHED. VALUE "+finish);
                if (finish != -1) {
                    if (finish == 1) {
                        uploadListener.onUploadFinished(true);
                    } else if (finish == 0) {
                        uploadListener.onUploadFinished(false);
                    }
                }
            }
        };

        LocalBroadcastManager.getInstance(context).registerReceiver(mMessageReceiver,
                new IntentFilter("SD_CARD_MANAGER"));
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "SD card mounted!");
        Log.d(TAG, "starting downloader service");
        Intent intent1 = new Intent(context, Downloader.class);

        if (intent.getAction().equals(Intent.ACTION_MEDIA_MOUNTED)) {
            context.startService(intent1);
        } else if (intent.getAction().equals(Intent.ACTION_MEDIA_UNMOUNTED)) {
//            context.stopService(intent);
        }
    }
}
